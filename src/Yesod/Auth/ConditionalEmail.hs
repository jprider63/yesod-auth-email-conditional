{-# LANGUAGE TupleSections, FlexibleContexts #-}
-- | Inspired by Yesod.Auth.Email

module Yesod.Auth.ConditionalEmail (
      authConditionalEmailPlugin
    , YesodAuthConditionalEmail(..)
    , ConditionalEmailFailure(..)
    , conditionalEmailChallengeRoute
    , makeConditionalEmailForm
    , makeConditionalEmailFormWithUsername
    ) where

import Control.Monad (when)
import qualified Crypto.Nonce as Nonce
import Data.Byteable (constEqBytes)
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8)
import Data.Time.Clock
import Yesod.Auth
import qualified Yesod.Auth.Message as Auth
import Yesod.Core
import Yesod.Form.Input (iopt, runInputPost)
-- import Yesod.Form.Bootstrap3
import Yesod.Form.Fields (textField)
-- import Yesod.Form.Functions

-- import Control.Monad
-- import Yesod.Core.Types

authConditionalEmailPlugin :: YesodAuthConditionalEmail site => AuthPlugin site
authConditionalEmailPlugin = AuthPlugin pluginName dispatch loginW
    where
        dispatch "GET" ["verify", userIdT, challenge] | Just userId <- fromPathPiece userIdT = liftSubHandler $ getAuthConditionalEmailChallengeR userId challenge
        dispatch "POST" ["challenge","send"] = liftSubHandler $ postAuthConditionalEmailSendChallengeR 
        dispatch _ _ = notFound
        loginW _ = makeConditionalEmailForm

class (YesodAuth site, Eq (AuthId site), RedirectUrl site (Route Auth)) => YesodAuthConditionalEmail site where
    -- | Specify whether the given user can authenticate via email. 
    allowEmailAuth :: AuthId site -> HandlerFor site Bool

    -- | Get the user for a given email/identity. 
    authEmailUser :: Text -> HandlerFor site (Maybe (AuthId site))

    -- | Send the authentication email to the user. When the user clicks on the link, the user will be authenticated.
    sendAuthEmail :: AuthId site -> Route site -> HandlerFor site ()

    -- | Set authentication challenge for a given user. 
    setEmailAuthChallenge :: AuthId site -> AuthChallenge -> UTCTime -> HandlerFor site ()

    -- | Lookup the authentication challenge for a given user. Return the authentication challenge and generation time. 
    lookupEmailAuthChallenge :: AuthId site -> HandlerFor site (Maybe (AuthChallenge, UTCTime))

    -- Delete the authentication challenge for a given user.
    deleteEmailAuthChallenge :: AuthId site -> HandlerFor site ()

    -- | Set the expiration period for challenges. The default is one day. Returning `Nothing` turns off the expiration. 
    authConditionalEmailExpirationTime :: HandlerFor site (Maybe Minutes)
    authConditionalEmailExpirationTime = return $ Just $ 60 * 24

    -- | Unique user identifier to put in `Creds`. There should be a better way to do this...
    authEmailUserIdent :: AuthId site -> HandlerFor site Text

    -- | Optional HTML to render when a challenge fails. The first argument specifies the failure reason. 
    authConditionalEmailFailure :: ConditionalEmailFailure -> SubHandlerFor Auth site Html
    authConditionalEmailFailure ConditionalEmailFailureExpired = do
        liftHandler $ setMessage [shamlet|
            <p .text-warning>
                This challenge has expired. Please try again. 
        |]
        redirect LoginR
    authConditionalEmailFailure ConditionalEmailFailureNotFound = do
        liftHandler $ setMessage [shamlet|
            <p .text-warning>
                Authentication failed. Please try again. 
        |]
        redirect LoginR
    authConditionalEmailFailure ConditionalEmailFailureUserNotAllowed = do
        liftHandler $ setMessage [shamlet|
            <p .text-warning>
                You cannot log in with an email challenge. 
        |]
        redirect LoginR

    -- | Optional method to generate a random challenge.
    generateChallenge :: HandlerFor site AuthChallenge
    generateChallenge = liftHandler $ Nonce.new >>= Nonce.nonce128urlT

    toMasterRoute :: Route Auth -> Route site

type AuthChallenge = Text
type Minutes = Int

data ConditionalEmailFailure = 
      ConditionalEmailFailureExpired -- ^ The challenge expired.
    | ConditionalEmailFailureNotFound -- ^ Did not find the challenge.
    | ConditionalEmailFailureUserNotAllowed -- ^ User is not allowed to use email auth according to `allowEmailAuth`.

-- 
getAuthConditionalEmailChallengeR :: (YesodAuthConditionalEmail site) => AuthId site -> AuthChallenge -> SubHandlerFor Auth site TypedContent
getAuthConditionalEmailChallengeR userId challenge' = do
    -- Check if logged in as same user.
    userIdM <- liftHandler maybeAuthId
    when (userIdM == Just userId) $ liftHandler $ do
        master <- getYesod
        redirect $ loginDest master

    -- Check if user can use email to authenticate. 
    allowed <- liftHandler $ allowEmailAuth userId
    if not allowed then
        fmap toTypedContent $ authConditionalEmailFailure ConditionalEmailFailureUserNotAllowed
    else do
        -- Lookup challenge.
        challengeM <- liftHandler $ lookupEmailAuthChallenge userId
        case challengeM of
            Nothing -> 
                fmap toTypedContent $ authConditionalEmailFailure ConditionalEmailFailureNotFound
                
            -- Check if the challenge doesn't match.
            Just (challenge, _) | not (constEqText challenge challenge') -> do
                fmap toTypedContent $ authConditionalEmailFailure ConditionalEmailFailureNotFound

            Just (_, genTime) -> do
                -- Check if expiration required.
                expirationMinutesM <- liftHandler $ authConditionalEmailExpirationTime
                now <- liftIO getCurrentTime
                case expirationMinutesM of
                    Nothing ->
                        -- No timeout so authenticate.
                        liftHandler $ authenticate userId
                    Just expirationMinutes' ->
                        let expirationMinutes = fromInteger $ toInteger expirationMinutes' in
                        if diffUTCTime now genTime > expirationMinutes * 60 then
                            fmap toTypedContent $ authConditionalEmailFailure ConditionalEmailFailureExpired
                        else
                            -- All checks passed so authenticate.
                            liftHandler $ authenticate userId
                    
    where
        authenticate userId = do
            ident <- authEmailUserIdent userId -- TODO: Figure out how to remove this..
            deleteEmailAuthChallenge userId
            setCredsRedirect $ Creds pluginName ident []

        constEqText a b = (encodeUtf8 a) `constEqBytes` (encodeUtf8 b)

emailKeyName :: Text
emailKeyName = "auth-email-conditional-email"

postAuthConditionalEmailSendChallengeR :: YesodAuthConditionalEmail site => SubHandlerFor Auth site TypedContent
postAuthConditionalEmailSendChallengeR = do
    emailM <- liftHandler $ runInputPost $ 
        iopt textField emailKeyName
    case emailM of
        Nothing ->
            loginErrorMessageI LoginR Auth.InvalidLogin
        Just email -> do
            -- Get user.
            userM <- liftHandler $ authEmailUser email
            case userM of
                Nothing ->
                    loginErrorMessageI LoginR Auth.InvalidLogin
                Just userId -> do
                    -- Check if the user is allowed to use email auth.
                    allowed <- liftHandler $ allowEmailAuth userId
                    if not allowed then
                        -- loginErrorMessageI LoginR Auth.InvalidLogin
                        fmap toTypedContent $ liftHandler $ onErrorHtml (toMasterRoute LoginR) "You cannot use email authentication."
                    else do
                        -- Generate challenge.
                        challenge <- liftHandler $ generateChallenge
                        toParent <- getRouteToParent
                        let route = toParent $ conditionalEmailChallengeRoute userId challenge 
                        now <- liftIO getCurrentTime

                        -- Store challenge.
                        liftHandler $ setEmailAuthChallenge userId challenge now

                        -- Send challenge.
                        liftHandler $ sendAuthEmail userId route

                        -- TODO: Fix this. Better text. Internationalized. XXX
                        loginErrorMessage (toParent LoginR) "Please check your email for a login link."

    -- where

    --     randomText :: (MonadIO m) => Int -> m Text
    --     randomText l = do
    --         stdgen <- liftIO newTFGen
    --         return $ Text.pack $ fst $ randomString l stdgen
    --     
    --     -- From mime-mail, MIT license by Michael Snoyman (http://hackage.haskell.org/package/mime-mail).
    --     -- Generates a random sequence of alphanumerics of the given length.
    --     randomString :: RandomGen d => Int -> d -> (String, d)
    --     randomString len =
    --         first (map toChar) . sequence' (replicate len (randomR (0, 61)))
    --       
    --     sequence' [] g = ([], g)
    --     sequence' (f:fs) g =
    --         let (f', g') = f g
    --             (fs', g'') = sequence' fs g'
    --          in (f' : fs', g'')

    --     toChar i
    --         | i < 26 = toEnum $ i + fromEnum 'A'
    --         | i < 52 = toEnum $ i + fromEnum 'a' - 26
    --         | otherwise = toEnum $ i + fromEnum '0' - 52

pluginName :: Text
pluginName = "YesodAuthConditionalEmail"

-- | Form submission target route.
-- conditionalEmailChallengeRoute :: forall site . YesodAuth site => AuthId site -> AuthChallenge -> AuthRoute
conditionalEmailChallengeRoute :: PathPiece userId => userId -> AuthChallenge -> AuthRoute
conditionalEmailChallengeRoute userId challenge = PluginR pluginName ["verify", toPathPiece userId, challenge]

conditionalEmailSendChallengeRoute :: AuthRoute
conditionalEmailSendChallengeRoute = PluginR pluginName ["challenge","send"]

-- handlerToWidget' :: Monad m => HandlerT child (HandlerT site m) a -> WidgetT child (WidgetT site m) a
-- handlerToWidget' (HandlerT f) = WidgetT $ liftM (, mempty) . f
-- 
-- makeConditionalEmailForm :: WidgetT Auth (WidgetFor site) ()
-- makeConditionalEmailForm = do
--     tp <- handlerToWidget $ handlerToWidget getRouteToParent
--     return ()

-- makeConditionalEmailForm :: YesodAuthConditionalEmail site => (Route Auth -> Route site) -> WidgetFor site ()
-- makeConditionalEmailForm tm = do
--     [whamlet|
--         <a href="@{tm conditionalEmailChallengeRoute}">test</a>
--     |]

makeConditionalEmailForm :: YesodAuthConditionalEmail site => WidgetFor site ()
makeConditionalEmailForm = do
    let tm = toMasterRoute
    -- (formW, formE) <- generateFormPost $ emailForm
    [whamlet|
        <form role=form method=post action=@{tm conditionalEmailSendChallengeRoute}>
            <div .form-group>
                <label for="#{emailKeyName}">
                    Email address
                <input type="email" class="form-control" name="#{emailKeyName}" id="#{emailKeyName}" placeholder="Email">

            <button type="submit" .btn .btn-default>
                Login request
    |]

makeConditionalEmailFormWithUsername :: YesodAuthConditionalEmail site => WidgetFor site ()
makeConditionalEmailFormWithUsername = do
    let tm = toMasterRoute
    -- (formW, formE) <- generateFormPost $ emailForm
    [whamlet|
        <form role=form method=post action=@{tm conditionalEmailSendChallengeRoute}>
            <div .form-group>
                <label for="#{emailKeyName}">
                    Username
                <input class="form-control" name="#{emailKeyName}" id="#{emailKeyName}" placeholder="Username">

            <button type="submit" .btn .btn-default>
                Login request
    |]

-- data EmailForm = EmailForm Text
-- 
-- -- emailForm :: FormRender m EmailForm
-- emailForm = renderBootstrap3 BootstrapBasicForm $ EmailForm 
--     <$> areq textField (bfs emailLabel) Nothing
-- 
--     where
--         emailLabel = "Email" :: Text

-- makeConditionalEmailForm :: YesodAuthConditionalEmail site => WidgetFor site ()
-- makeConditionalEmailForm = do
--     liftWidgetT w
--     
--     where
--         w :: WidgetT Auth (WidgetFor site) ()
--         w = [whamlet|
--                 <a href="@{conditionalEmailChallengeRoute}">test</a>
--             |]
